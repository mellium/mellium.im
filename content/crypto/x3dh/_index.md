+++
title    = "x3dh"
date     = "2024-04-30T21:07:40-04:00"
repo     = "https://mellium.im/repo/crypto"
todo     = "https://mellium.im/issue"
category = ""
+++

Package **`x3dh`** implements the [X3DH key agreement protocol][x3dh].

It also includes related functionality for converting between [ED25519]/[EdDSA]
keys and Elliptic Curve Diffie-Hellman (ECDH) keys over the [X25519] curve.

[x3dh]: https://signal.org/docs/specifications/x3dh/
[ED25519]: https://ed25519.cr.yp.to/
[EdDSA]: https://www.rfc-editor.org/rfc/rfc8032.html
[X25519]: https://www.rfc-editor.org/rfc/rfc7748.html#section-5
