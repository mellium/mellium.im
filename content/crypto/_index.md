+++
title    = "crypto"
date     = "2024-04-30T21:07:40-04:00"
repo     = "https://mellium.im/repo/crypto"
todo     = "https://mellium.im/issue"
category = "Crypto"
summary  = "Package `crypto` implements various cryptographic algorithms."
+++

Package **`crypto`** implements various cryptographic algorithms.

As a general rule of thumb crypto packages should be placed here if they would
be useful in other contexts besides the Mellium XMPP libraries.
If the crypto you want to implement is specific to XMPP it should live in
[`mellium.im/xmpp/crypto`] instead.

[`mellium.im/xmpp/crypto`]: https://mellium.im/xmpp/crypto/
