+++
title    = "blogsync"
date     = "2019-05-26T12:08:30-05:00"
repo     = "https://codeberg.org/mellium/blogsync"
todo     = "https://codeberg.org/mellium/xmpp/issues"
category = "Unmaintained"
summary = "The blogsync command is a tool for importing and exporting blog posts between Markdown files and write.as."
+++

The **`blogsync`** command is a tool for importing and exporting blog posts
between Markdown files and [write.as].

It is compatible with files created by popular static site generators such as
Hugo and Jekyll.

[write.as]: https://write.as/
