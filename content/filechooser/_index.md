+++
title    = "filechooser"
date     = "2024-10-21T13:18:19-04:00"
repo     = "https://mellium.im/repo/filechooser"
todo     = "https://codeberg.org/mellium/filechooser/issues"
category = "Other"
+++

Package **`filechooser`** contains tview widgets for selecting files.
