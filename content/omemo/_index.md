+++
title    = "omemo"
date     = "2023-08-27T12:00:00-05:00"
repo     = "https://mellium.im/repo/omemo"
todo     = "https://mellium.im/issue"
category = "Crypto"

[[xeps]]
  resource = "https://xmpp.org/extensions/xep-0384.html"
  version  = "0.8.3"
[[xeps]]
  resource = "https://xmpp.org/extensions/attic/xep-0384-0.3.0.html"
  version  = "0.3.0"
+++

Package **`omemo`** is an attempt to implement the OMEMO end-to-end encryption
protocol for [`mellium.im/xmpp`].

[`mellium.im/xmpp`]: https://mellium.im/xmpp
