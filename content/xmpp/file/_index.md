+++
category = ""

[[xeps]]
  resource = "https://xmpp.org/extensions/xep-0446.html"
  status   = "complete"
  version  = "0.2.0"
+++

Package **`file`** contains shared functionality between various file transfer
mechanisms.
