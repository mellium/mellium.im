+++
date     = "2024-05-01T15:22:45-04:00"
category = ""

[[xeps]]
  resource = "https://xmpp.org/extensions/xep-0300.html"
  status   = "complete"
  version  = "1.0.0"
+++

Package **`crypto`** contains common cryptographic elements that are used by
XMPP.

This package is for XMPP specific cryptographic primitives and elements.
As a general rule of thumb, if the cryptography you want to implement would be
usable by other projects that have nothing to do with XMPP it belongs in
[`mellium.im/crypto`] instead.

[`mellium.im/crypto`]: https://mellium.im/crypto/
