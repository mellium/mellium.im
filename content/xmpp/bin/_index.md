+++
category = ""

[[xeps]]
  resource = "https://xmpp.org/extensions/xep-0231.html"
  status   = "full"
  version  = "1.1"
+++

Package **`bin`** contains a simple.mechanism for requesting small snippets of
binary data.
