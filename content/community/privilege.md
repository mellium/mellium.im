+++
title    = "Privilege"
date     = "2023-02-24T14:26:07-05:00"
category = "Maintained"
+++

This module implements [XEP-0356: Privileged Entity].

[XEP-0356: Privileged Entity]: https://xmpp.org/extensions/xep-0356.html
