+++
title    = "Community"
date     = "2023-02-24T14:26:07-05:00"
nodocs   = true
noimport = true
+++

These modules were created by the community and requested an import path under
the `mellium.im/community` namespace.
They may or may not be maintained and may have different levels of code quality
than the main projects.

Some of the community modules may be moved into the main project at a later
date, in which case a backwards compatible wrapper will be provided in line with
Go best practices.
